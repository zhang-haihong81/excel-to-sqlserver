﻿using SQLite;

using System;

namespace ExcelToSqlserver
{
    internal class TaskInfo
    {
        public string Code { get; set; }
        public string Detail { get; set; }
        public bool Done { get; set; }
        public DateTime End { get; set; }

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        public bool ShouldDo { get; set; }
        public DateTime Start { get; set; }
    }
}