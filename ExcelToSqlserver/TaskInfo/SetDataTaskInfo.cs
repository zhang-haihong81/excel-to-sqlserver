﻿using MiniExcelLibs;

using System;

namespace ExcelToSqlserver
{
    internal class ColumnInfo
    {
        public ColumnInfo(string name, int length = 0, Type type = null)
        {
            Name = name;
            Length = length;
            Type = type;
        }

        public int Length { get; set; }
        public string Name { get; set; }
        public Type Type { get; set; }
    }

    internal class SetXlsxDataTaskInfo : TaskInfo
    {
        private string file;
        private string sheet;
        private string table;
        private string startCell;
        private string stopKeyString;

        public SetXlsxDataTaskInfo()
        { }

        //csv文件裁剪，遍历rows时 若遇到 csvStopKeyString 则停止
        public SetXlsxDataTaskInfo(string excelFilePath, string sheetName, string databaseTableName, string startCell, string stopKeyString = "")
        {
            File = excelFilePath;
            Sheet = sheetName;
            Table = databaseTableName;
            StartCell = startCell;
            StopKeyString = stopKeyString;
        }

        public string File
        { get { return file; } set { file = value.Trim(); } }

        public string Sheet
        { get { return sheet; } set { sheet = value.Trim(); } }

        public string StartCell
        { get { return startCell; } set { startCell = value.Trim(); } }

        public string Table
        { get { return table; } set { table = value.Trim(); } }

        public string StopKeyString { get => stopKeyString; set => stopKeyString = value.Trim(); }

        public override string ToString()
        {
            return $"{Id}\t{Code}\t{File}\t{Sheet}\t{StartCell}\t{Table}\t{ShouldDo}\t{Done}\t{Detail}\t{Start}\t{End}";
        }
    }

    internal class SetDataTaskInfo : TaskInfo
    {
        private string file;
        private string sheet;
        private string table;
        private string startCell;
        private string stopKeyString;
        private ExcelType excelType;
        private IConfiguration configuration;
        private char seperator;

        public SetDataTaskInfo()
        { }

        //csv文件裁剪，遍历rows时 若遇到 csvStopKeyString 则停止
        public SetDataTaskInfo(string excelFilePath, string sheetName, string databaseTableName, string startCell, string stopKeyString = "", string seperator = ",")
        {
            File = excelFilePath;
            Sheet = sheetName;
            Table = databaseTableName;
            StartCell = startCell;
            StopKeyString = stopKeyString;
            excelType = File.EndsWith(".csv", StringComparison.OrdinalIgnoreCase) ? ExcelType.CSV : ExcelType.XLSX;
            switch (seperator)
            {
                case ",": Seperator = ','; break;
                case ";": Seperator = ';'; break;
                case "\t": Seperator = '\t'; break;
                case " ": Seperator = ' '; break;
                default: Seperator = ','; break;
            }
            switch (excelType)
            {
                case ExcelType.CSV:
                    configuration = new MiniExcelLibs.Csv.CsvConfiguration()
                    {
                        //StreamReaderFunc = (stream) => new StreamReader(stream, encoding),
                        Seperator = Seperator
                    };
                    break;
                case ExcelType.XLSX:
                case ExcelType.UNKNOWN:
                default:
                    configuration = new MiniExcelLibs.OpenXml.OpenXmlConfiguration();
                    break;
            }
        }

        public string File
        { get { return file; } set { file = value.Trim(); } }

        public string Sheet
        { get { return sheet; } set { sheet = value.Trim(); } }

        public string StartCell
        { get { return startCell; } set { startCell = value.Trim(); } }

        public string Table
        { get { return table; } set { table = value.Trim(); } }

        public string StopKeyString { get => stopKeyString; set => stopKeyString = value.Trim(); }
        public char Seperator { get => seperator; set => seperator = value; } //待是设置
        public ExcelType ExcelType { get => File.EndsWith(".csv", StringComparison.OrdinalIgnoreCase) ? ExcelType.CSV : ExcelType.XLSX; set => excelType = value; }
        public IConfiguration Configuration { get => configuration; set => configuration = value; }
        public override string ToString()
        {
            return $"{Id}\t{Code}\t{File}\t{Sheet}\t{StartCell}\t{Table}\t{ShouldDo}\t{Done}\t{Detail}\t{Start}\t{End}";
        }
    }
}