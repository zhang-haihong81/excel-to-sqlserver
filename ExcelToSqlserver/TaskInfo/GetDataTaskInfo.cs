﻿namespace ExcelToSqlserver
{
    internal class GetDataTaskInfo : TaskInfo
    {
        private string file;
        private string sheet;
        private string sql;
        private string sqlFile;

        public GetDataTaskInfo()
        { }

        public GetDataTaskInfo(string excelFilePath, string sheetName, string sql, string sqlFile)
        {
            File = excelFilePath;
            Sheet = sheetName;
            Sql = sql;
            SqlFile = sqlFile;
        }

        public string File
        { get { return file; } set { file = value.Trim(); } }

        public string Sheet
        { get { return sheet; } set { sheet = value.Trim(); } }

        public string Sql
        { get { return sql; } set { sql = value.Trim(); } }

        public string SqlFile
        { get { return sqlFile; } set { sqlFile = value.Trim(); } }

        public override string ToString()
        {
            return $"{Id}\t{Code}\t{File}\t{Sheet}\t{Sql}\t{SqlFile}\t{ShouldDo}\t{Done}\t{Detail}\t{Start}\t{End}";
        }
    }
}