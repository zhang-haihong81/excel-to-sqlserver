﻿using System;

namespace ExcelToSqlserver.Helpers
{
    internal static class ConsoleHelper
    {
        public static void ShowException(string message)
        {
            ShowInForegroundColor(message, ConsoleColor.Red);
        }

        public static void ShowInForegroundColor(string message, ConsoleColor color)
        {
            var precolor = Console.ForegroundColor;
            Console.ForegroundColor = color;
            Console.WriteLine(message);
            Console.ForegroundColor = precolor;
        }

        public static void ShowSuccess(string message)
        {
            ShowInForegroundColor(message, ConsoleColor.Green);
        }

        internal static void ShowCommand(string message)
        {
            ShowInForegroundColor(message, ConsoleColor.Yellow);
        }
    }
}