﻿using System.IO;

namespace ExcelToSqlserver.Helpers
{
    internal class XlsxFileHelper
    {
        public string GetCutedFile(string originFile, int startRow, string stopKeyString = "", bool verySmall = false, string folderPath = null)
        {
            if (!File.Exists(originFile)) return null;

            if (string.IsNullOrEmpty(folderPath))
            {
                folderPath = Path.Combine(Path.GetDirectoryName(originFile), "CutedFiles");
            }
            Directory.CreateDirectory(folderPath);
            var checkstopKeyString = !string.IsNullOrEmpty(stopKeyString);
            var encoding = TxtFileEncoder.GetEncoding(originFile);
            string cutedCsvfile = Path.Combine(folderPath, Path.GetFileName(originFile)
                                        .Replace(".xlsx", "-cuted.xlsx").Replace(".csv", "-cuted.csv"));
            FileStream originfs = new FileStream(originFile, FileMode.Open, FileAccess.Read);
            StreamReader streamReader = new StreamReader(originfs, encoding);
            FileStream cutedfs = new FileStream(cutedCsvfile, FileMode.Create, FileAccess.Write);
            StreamWriter streamWriter = new StreamWriter(cutedfs, encoding);
            int i = 1;
            while (!streamReader.EndOfStream)
            {
                string PeekContent = streamReader.ReadLine();
                if (i++ < startRow) continue;
                if (verySmall)
                {
                    if (i > 100 + startRow)
                    {
                        break;
                    }
                }
                if (checkstopKeyString && PeekContent.Contains(stopKeyString)) break;
                streamWriter.Write(PeekContent + "\r\n");
            }
            streamWriter.Flush();
            streamWriter.Close();
            streamReader.Close();
            return cutedCsvfile;
        }
    }
}