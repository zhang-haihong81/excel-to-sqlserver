﻿using System.Data;
using System.Data.SqlClient;

namespace ExcelToSqlserver.Helpers
{
    internal static class SQLHelper
    {
        //public static string connectionString { get; set; }
        public static bool CanConnectToSQLServer(string connectionString)
        {
            using (SqlConnection sqlconn = new SqlConnection(connectionString))
            {
                try
                {
                    sqlconn.Open();
                    if (sqlconn.State == ConnectionState.Open)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (System.Exception ex)
                {
                    ConsoleHelper.ShowException(ex.Message + " " + connectionString);
                    return false;
                }
            }
        }

        //private static string connectionStr = ConfigurationManager.ConnectionStrings["connectionStr"].ConnectionString;

        ///// <summary>
        ///// 增删改
        ///// </summary>
        ///// <param name="commandText"></param>
        ///// <param name="parameters"></param>
        ///// <returns></returns>
        //public static int ExcuteModify(string commandText, params SqlParameter[] parameters)
        //{
        //    try
        //    {
        //        using (SqlConnection sqlConnection = new SqlConnection(connectionStr))
        //        {
        //            sqlConnection.Open();
        //            using (SqlCommand cmd = new SqlCommand(commandText, sqlConnection))
        //            {
        //                cmd.Parameters.AddRange(parameters);
        //                int result = cmd.ExecuteNonQuery();
        //                return result;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }
        //}

        ///// <summary>
        ///// 查询=>返回第一行第一列
        ///// </summary>
        ///// <param name="commandText"></param>
        ///// <param name="parameters"></param>
        ///// <returns></returns>
        //public static object ExcuteScalar(string commandText, params SqlParameter[] parameters)
        //{
        //    try
        //    {
        //        using (SqlConnection sqlConnection = new SqlConnection(connectionStr))
        //        {
        //            sqlConnection.Open();
        //            using (SqlCommand cmd = new SqlCommand(commandText, sqlConnection))
        //            {
        //                cmd.Parameters.AddRange(parameters);
        //                return cmd.ExecuteScalar();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }
        //}

        ///// <summary>
        ///// 返回DataTable(Select) <返回结果比较少的>
        ///// </summary>
        ///// <param name="sql"></param>
        ///// <returns></returns>
        //public static DataTable ExecuteDataTable(string sql, params SqlParameter[] parameters)
        //{
        //    using (SqlConnection conn = new SqlConnection(connectionStr))
        //    {
        //        conn.Open();
        //        using (SqlCommand cmd = conn.CreateCommand())
        //        {
        //            cmd.CommandText = sql;
        //            cmd.Parameters.AddRange(parameters);

        //            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
        //            DataSet dataset = new DataSet();
        //            adapter.Fill(dataset);
        //            return dataset.Tables[0];
        //        }
        //    }
        //}

        ///// <summary>
        ///// 基于序号的查询(Select)
        ///// </summary>
        ///// <param name="sql"></param>
        ///// <param name="parameter"></param>
        ///// <returns></returns>
        //public static List<string> ExecuteDataReader(string sql, int columnIndex, params SqlParameter[] parameters)
        //{
        //    using (SqlConnection conn = new SqlConnection(connectionStr))
        //    {
        //        conn.Open();
        //        using (SqlCommand cmd = conn.CreateCommand())
        //        {
        //            cmd.CommandText = sql;
        //            cmd.Parameters.AddRange(parameters);

        //            List<string> list = new List<string>();
        //            using (SqlDataReader reader = cmd.ExecuteReader())
        //            {
        //                while (reader.Read())
        //                {
        //                    list.Add(reader.GetString(columnIndex));
        //                }
        //                return list;
        //            }
        //        }
        //    }
        //}

        //public static void ExcuteSingleGetDataTask(string connString, GetDataTaskInfo task)
        //{
        //    using (SqlConnection sqlconn = new SqlConnection(connString))
        //    {
        //        sqlconn.Open();
        //        SqlCommand command = sqlconn.CreateCommand();
        //        command.CommandText = task.Sql;
        //        SqlDataReader reader = command.ExecuteReader();
        //        MiniExcel.SaveAs(task.File, reader, true, task.Sheet, ExcelType.XLSX);
        //        sqlconn.Close();
        //        Console.WriteLine($"{task.File} {task.Sheet}  生成完毕！");
        //    }
        //}

        public static void ExcuteSql(string connString, string strSql)
        {
            try
            {
                using (SqlConnection sqlconn = new SqlConnection(connString))
                {
                    sqlconn.Open();
                    SqlCommand command = sqlconn.CreateCommand();
                    command.CommandText = strSql;
                    command.ExecuteNonQuery();
                    sqlconn.Close();
                }
            }
            catch (System.Exception ex)
            {
                ConsoleHelper.ShowException(ex.Message);
                throw;
            }
        }

        //public static void ExcuteSqlFile(string connString, string sqlFile)
        //{
        //    if (File.Exists(sqlFile))
        //    {
        //        using (SqlConnection sqlconn = new SqlConnection(connString))
        //        {
        //            sqlconn.Open();
        //            SqlCommand command = sqlconn.CreateCommand();
        //            command.CommandText = File.ReadAllText(sqlFile);
        //            command.ExecuteNonQuery();
        //            sqlconn.Close();
        //        }
        //    }
        //    else
        //    {
        //        Console.WriteLine(sqlFile + " 不存在！");
        //    }
        //}

        //public static void SaveSqlFileResultToExcel(string connString, string sqlFile, string excelFileName, string sheetName)
        //{
        //    if (!File.Exists(sqlFile))
        //    {
        //        Console.WriteLine(sqlFile + " 不存在！将退出本次任务！");
        //        return;
        //    }
        //    var excel = excelFileName;
        //    if (File.Exists(excel))
        //    {
        //        Console.WriteLine(excel + " 已存在！将退出本次任务！");
        //        return;
        //    }
        //    var sql = File.ReadAllText(sqlFile);
        //    SaveSqlResultToExcel(connString, excel, sheetName, sql);
        //}

        //public static void SaveSqlResultToExcel(string connString, string excelFile, string sheetName, string sql)
        //{
        //    using (SqlConnection sqlconn = new SqlConnection(connString))
        //    {
        //        sqlconn.Open();
        //        SqlCommand command = sqlconn.CreateCommand();
        //        command.CommandText = sql;
        //        SqlDataReader reader = command.ExecuteReader();

        //        MiniExcel.SaveAs(excelFile, reader, true, sheetName, ExcelType.XLSX);
        //        sqlconn.Close();
        //        Console.WriteLine(excelFile + " 生成完毕！");
        //    }
        //}
    }
}