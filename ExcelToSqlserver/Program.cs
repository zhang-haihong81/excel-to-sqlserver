﻿using ExcelToSqlserver.Helpers;

using System;
using System.IO;
using System.Linq;
using System.Text;

namespace ExcelToSqlserver
{
    // todo 不同平台的csv格式不同，怎么办？ 暂时打开保存是一个办法。有些慢。
    internal class Program
    {
        private static void CreateXlsxTableSql(string[] args)
        {
            SetXlsxDataTaskManager manager = new SetXlsxDataTaskManager();
            if (args.Length == 4)
            {
                SetXlsxDataTaskInfo task = new SetXlsxDataTaskInfo(args[1], args[2], args[3], "A1");
                if (!File.Exists(task.File)) { Console.WriteLine(task.File + "不存在！"); return; }
                Console.WriteLine("\n" + manager.GenerateCreateTableSql(task));
            }
            else if (args.Length == 5)
            {
                SetXlsxDataTaskInfo task = new SetXlsxDataTaskInfo(args[1], args[2], args[3], args[4]);
                if (!File.Exists(task.File)) { Console.WriteLine(task.File + "不存在！"); return; }
                Console.WriteLine("\n" + manager.GenerateCreateTableSql(task));
            }
            else
            {
                StringBuilder stringBuilder = new StringBuilder();
                manager.Tasks.Where(t => t.ShouldDo).ToList().ForEach(t =>
                {
                    if (!File.Exists(t.File))
                    { Console.WriteLine(t.File + "不存在！"); }
                    else
                    { stringBuilder.Append("\n" + manager.GenerateCreateTableSql(t)); }
                }); //生成全部脚本
                Console.WriteLine(stringBuilder.ToString());
            }
            ReadKeyAndReturn();
        }

        private static void CreateTableSql(string[] args)
        {
            SetDataTaskManager manager = new SetDataTaskManager();
            if (args.Length == 4)
            {
                SetDataTaskInfo task = new SetDataTaskInfo(args[1], args[2], args[3], "A1");
                if (!File.Exists(task.File)) { Console.WriteLine(task.File + "不存在！"); return; }
                Console.WriteLine("\n" + manager.GenerateCreateTableSql(task));
            }
            else if (args.Length == 5)
            {
                SetDataTaskInfo task = new SetDataTaskInfo(args[1], args[2], args[3], args[4]);
                if (!File.Exists(task.File)) { Console.WriteLine(task.File + "不存在！"); return; }
                Console.WriteLine("\n" + manager.GenerateCreateTableSql(task));
            }
            else
            {
                StringBuilder stringBuilder = new StringBuilder();
                manager.Tasks.Where(t => t.ShouldDo).ToList().ForEach(t =>
                {
                    if (!File.Exists(t.File))
                    { Console.WriteLine(t.File + "不存在！"); }
                    else
                    { stringBuilder.Append("\n" + manager.GenerateCreateTableSql(t)); }
                }); //生成全部脚本
                Console.WriteLine(stringBuilder.ToString());
            }
            ReadKeyAndReturn();
        }

        private static void GetAllCutedCsvFiles()
        {
            SetDataTaskManager manager = new SetDataTaskManager();
            Console.WriteLine("\n任务列表: ");
            manager.Tasks.ForEach(g => Console.WriteLine(string.Join("\t", g.Id, g.File, g.Sheet, g.StartCell, g.StopKeyString, g.ShouldDo)));
            Console.WriteLine("\n任务开始：" + DateTime.Now.ToString());
            manager.GetAllCutedCsvFiles();
            Console.WriteLine("\n任务结束：" + DateTime.Now.ToString() + "\n");
        }

        private static void GetDataFromDb()
        {
            GetDataTaskManager manager = new GetDataTaskManager();
            Console.WriteLine("\n任务列表: ");
            manager.Tasks.ForEach(g => Console.WriteLine(string.Join("\t", g.Id, g.File, g.Sheet, g.Sql, g.SqlFile, g.ShouldDo)));
            Console.WriteLine("\n任务开始：" + DateTime.Now.ToString());
            manager.ExcuteTasks();
            Console.WriteLine("\n任务结束：" + DateTime.Now.ToString() + "\n");
        }
        private static void Main(string[] args)
        {
            //Console.WriteLine(DateTime.Now.ToString() + "\n");
            //var cuted = SetDataTaskManager.CutCsvFile(@"E:\exceltest\20885310831095210156_202312_账务明细_5.csv", 5, @"#-----------------------------------------账务明细列表结束------------------------------------");
            //Console.WriteLine(DateTime.Now.ToString() + "\n");

            //命令行参数 a 裁剪全部CSV文件 根据开始单元格行，停止关键字csvStopKeyString
            var getAllCutedCsvFiles = args.Any(b => b.Equals("a"));

            //命令行参数 c 是否根据Excel生成创建表的模板文件
            var createXlsxTableSql = args.Any(b => b.Equals("c"));


            //命令行参数 cc 是否根据Excel生成创建表的模板文件
            var createCsvTableSql = args.Any(b => b.Equals("cc"));

            //命令行参数 -g 根据Tasks.xlsx中的GetDataTasks表，提取数据库数据到Excel。默认为false
            var getData = args.Any(b => b.Equals("g"));

            //命令行参数 -d 不创建表直接导入
            var creatTable = args.Any(b => b.Equals("d"));

            //命令行参数 -s 根据Tasks.xlsx中的SetDataTasks表，导入Excel数据到数据库表。默认为false
            var setData = args.Any(b => b.Equals("s"));

            //命令行参数 -v 显示任务拷贝行进度（100000行通知一次）。默认为false
            var showProcess = args.Any(b => b.Equals("v"));

            //命令行参数 -dc 不创建表直接导入
            var creatCsvTable = args.Any(b => b.Equals("dc"));

            //命令行参数 -sc 根据Tasks.xlsx中的SetDataTasks表，导入Excel数据到数据库表。默认为false
            var setCsvData = args.Any(b => b.Equals("sc"));

            //命令行参数 -vc 显示任务拷贝行进度（100000行通知一次）。默认为false
            var showCsvProcess = args.Any(b => b.Equals("vc"));

            //命令行参数 -t 显示任务历史。默认为false
            var showXlsxTasksHistory = args.Any(b => b.Equals("t"));

            //命令行参数 -tc 显示任务历史。默认为false
            var showCsvTasksHistory = args.Any(b => b.Equals("tc"));

            //命令行参数 -h 显示帮助。默认为false
            var showHelp = args.Any(b => b.Equals("h"));

            if (args.Length == 0)  //直接执行时提示命令左右
            {
                ShowHelp();
                var exitnow = false;

                while (!exitnow)
                {
                    Console.WriteLine("----------请输入要执行的命令字母（按e退出）：------------");
                    var command = Console.ReadKey(true).KeyChar.ToString();
                    ConsoleHelper.ShowCommand("开始执行命令：" + command);
                    switch (command)
                    {
                        case "a":
                            GetAllCutedCsvFiles();
                            break;

                        case "c":
                            CreateXlsxTableSql(args);
                            break;

                        case "cc":
                            CreateTableSql(args);
                            break;

                        case "s":
                            SetDataToDb(true, false);
                            break;

                        case "d":
                            SetDataToDb(false, false);
                            break;

                        case "v":
                            SetDataToDb(false, true);
                            break;

                        case "sc":
                            SetCsvDataToDb(true, false);
                            break;

                        case "dc":
                            SetCsvDataToDb(false, false);
                            break;

                        case "vc":
                            SetCsvDataToDb(false, true);
                            break;

                        case "g":
                            GetDataFromDb();
                            break;

                        case "t":
                            ShowXlsxTasksHistory(args);
                            break;

                        case "tc":
                            ShowCsvTasksHistory(args);
                            break;

                        case "e":
                            exitnow = true;
                            break;

                        case "h":
                            ShowHelp();
                            break;

                        default:
                            continue;
                    }
                }
            }

            if (getAllCutedCsvFiles) // 参数 a
            {
                GetAllCutedCsvFiles();
                ReadKeyAndReturn();
            }

            if (createXlsxTableSql) // 参数 -c 导入Excel数据到sqlserver的准备工作，根据excel生成创建表脚本
            {
                CreateXlsxTableSql(args);
                ReadKeyAndReturn();
            }

            if (createCsvTableSql) // 参数 -cc 导入Excel数据到sqlserver的准备工作，根据excel生成创建表脚本
            {
                CreateTableSql(args);
                ReadKeyAndReturn();
            }

            if (getData)// 参数 g
            {
                GetDataFromDb();
                ReadKeyAndReturn();
            }

            if (setData)// 参数 s
            {
                SetDataToDb(true, showProcess);
                ReadKeyAndReturn();
            }

            if (showProcess)// 参数 v
            {
                SetDataToDb(false, true);
                ReadKeyAndReturn();
            }

            if (creatTable)// 参数 d
            {
                SetDataToDb(false, showProcess);
                ReadKeyAndReturn();
            }

            if (setCsvData)// 参数 sc
            {
                SetCsvDataToDb(true, showProcess);
                ReadKeyAndReturn();
            }

            if (showCsvProcess)// 参数 vc
            {
                SetCsvDataToDb(false, true);
                ReadKeyAndReturn();
            }

            if (creatCsvTable)// 参数 dc
            {
                SetCsvDataToDb(false, showProcess);
                ReadKeyAndReturn();
            }

            if (showHelp) { ShowHelp(); return; }
            if (showXlsxTasksHistory)// 参数 t
            {
                ShowXlsxTasksHistory(args);
                ReadKeyAndReturn();
            }
            if (showCsvTasksHistory)// 参数 tc
            {
                ShowCsvTasksHistory(args);
                ReadKeyAndReturn();
            }
            return;
        }

        private static void ReadKeyAndReturn()
        {
            Console.WriteLine("按任意键继续。。。");
            Console.ReadKey();
            return;
        }

        private static void SetDataToDb(bool creatTable, bool showProcess)
        {
            SetXlsxDataTaskManager manager = new SetXlsxDataTaskManager();
            Console.WriteLine("\n任务列表: ");
            manager.Tasks.ForEach(s => Console.WriteLine(string.Join("\t", s.Id, s.File, s.Sheet, s.StartCell, s.Table, s.ShouldDo)));
            Console.WriteLine("\n任务开始：" + DateTime.Now.ToString());
            manager.ExcuteTasks(creatTable, showProcess);
            Console.WriteLine("\n任务结束：" + DateTime.Now.ToString() + "\n");
        }

        private static void SetCsvDataToDb(bool creatTable, bool showProcess)
        {
            SetDataTaskManager manager = new SetDataTaskManager();
            Console.WriteLine("\n任务列表: ");
            manager.Tasks.ForEach(s => Console.WriteLine(string.Join("\t", s.Id, s.File, s.Sheet, s.StartCell, s.Table, s.ShouldDo)));
            Console.WriteLine("\n任务开始：" + DateTime.Now.ToString());
            manager.ExcuteTasks(creatTable, showProcess);
            Console.WriteLine("\n任务结束：" + DateTime.Now.ToString() + "\n");
        }
        private static void ShowHelp()
        {
            Console.WriteLine(

            "\n使用帮助：\n\n" +
            "   首先，在 ExcelToSqlserver.exe.config 中配置SqlServer数据库连接字符串。\n\n" +

            "s 根据 Tasks.xlsm 文件中的【SetXlsxDataTasks】表列出的ShouldDo为1的任务，导入Excel数据到数据库表。\n" +
            "   1 有创建表模板则使用模板，没有则根据Excel生成脚本；\n" +
            "   2 执行创建表脚本（数据库有表，则不创建）；\n" +
            "   3 导入Excel表到数据库；\n" +
            "   4 若添加参数 -d ， 则1和2不执行，直接执行3\n\n" +
            "d 直接导入【SetDataTasks】中的Excel表到数据库（要求数据库有表）,跳过根据Excel生成创建表脚本步骤。\n\n" +

            "v 显示导入【SetDataTasks】中Excel到数据库表的进度行数。\n\n" +

           "sc 根据 Tasks.xlsm 文件中的【SetCsvDataTasks】表列出的ShouldDo为1的任务，导入Excel数据到数据库表。\n" +
            "   1 有创建表模板则使用模板，没有则根据Excel生成脚本；\n" +
            "   2 执行创建表脚本（数据库有表，则不创建）；\n" +
            "   3 导入Excel表到数据库；\n" +
            "   4 若添加参数 -d ， 则1和2不执行，直接执行3\n\n" +

            "dc 直接导入【SetCsvDataTasks】中的Excel表到数据库（要求数据库有表）,跳过根据Excel生成创建表脚本步骤。\n\n" +

            "vc 显示导入【SetCsvDataTasks】中Excel到数据库表的进度行数。\n\n" +

            "g 根据 Tasks.xlsm 文件中的 【GetDataTasks】 表列出的ShouldDo为1的任务，提取数据库数据保存到Excel。\n\n" +

            "a 对 Tasks.xlsm 文件中的 【SetCsvDataTasks】 表列出的 ShouldDo 为1的CSV文件，进行裁剪形成新csv文件，从 startCell 所在行开始到包含 CsvStopKeyString 的行结束。\n\n" +

            "c 根据【SetDataTasks】要执行任务中的Excel生成创建表脚本,并显示。\n" +
            "   命令行执行时 可跟随参数 ExcelFilePath, SheetName, DatabaseTableName, startCell，若无，则创建所有Task的创建表脚本。\n\n" +

            "cc 根据【SetCsvDataTasks】要执行任务中的CSV文件生成创建表脚本,并显示。\n" +
            "   命令行执行时 可跟随参数 ExcelFilePath, SheetName, DatabaseTableName, startCell，若无，则创建所有Task的创建表脚本。\n\n" +

            "t 显示曾经执行过的全部任务\n" +
             "  命令行执行时 可跟随参数进行筛选 依次为：DatabaseTableName, 任务Code, ExcelFilePath, SheetName, 是否完成Done，\n\n" +

            "h 显示本帮助信息。\n\n"
            );
        }

        private static void ShowXlsxTasksHistory(string[] args)
        {
            string table = "", code = "", file = "", sheet = "";
            bool done = true;
            SetXlsxDataTaskManager manager = new SetXlsxDataTaskManager();
            if (args.Length >= 2) { table = args[1]; }
            if (args.Length >= 3) { code = args[2]; }
            if (args.Length >= 4) { file = args[3]; }
            if (args.Length >= 5) { sheet = args[4]; }
            if (args.Length >= 6) { done = bool.Parse(args[5]); }
            manager.ShowTasksHistory(table, code, file, sheet, done);
        }

        private static void ShowCsvTasksHistory(string[] args)
        {
            string table = "", code = "", file = "", sheet = "";
            bool done = true;
            SetDataTaskManager manager = new SetDataTaskManager();
            if (args.Length >= 2) { table = args[1]; }
            if (args.Length >= 3) { code = args[2]; }
            if (args.Length >= 4) { file = args[3]; }
            if (args.Length >= 5) { sheet = args[4]; }
            if (args.Length >= 6) { done = bool.Parse(args[5]); }
            manager.ShowTasksHistory(table, code, file, sheet, done);
        }
    }
}