﻿using SQLite;

using System.Collections.Generic;
using System.Configuration;

namespace ExcelToSqlserver
{
    internal class TaskManager<T> where T : TaskInfo
    {
        public List<T> Tasks = new List<T>();
        protected string ExcelSheetName;
        protected SQLiteConnection SqliteDb;
        protected string SqlServerConnString;
        protected string TasksExcelFile;

        public TaskManager()
        {
            Prepare();
        }

        public virtual void ExcuteTasks()
        { }

        protected void Prepare()
        {
            TasksExcelFile = ConfigurationManager.AppSettings["TasksExcel"];
            SqlServerConnString = ConfigurationManager.ConnectionStrings["sqlserver"].ConnectionString;
            var SQLiteDbPath = ConfigurationManager.ConnectionStrings["sqlite"].ConnectionString;
            SqliteDb = new SQLiteConnection(SQLiteDbPath);
        }
    }
}