﻿using ExcelToSqlserver.Helpers;

using MiniExcelLibs;

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using System.IO;
using System.Linq;

namespace ExcelToSqlserver
{
    // Excel sheet ==> Database Table
    internal class SetDataTaskManager : TaskManager<SetDataTaskInfo>
    {
        protected string SqlsPath;
        private string defaultTableName = "THETABLENAME";

        public SetDataTaskManager() : base()
        {
            string basePath = AppDomain.CurrentDomain.BaseDirectory;
            SqlsPath = Path.Combine(basePath, "Sqls");
            ExcelSheetName = "SetDataTasks";
            Tasks = MiniExcel.Query<SetDataTaskInfo>(Path.Combine(basePath, TasksExcelFile), ExcelSheetName).Where(t => t.ShouldDo).ToList();
            SqliteDb.CreateTable<SetDataTaskInfo>();
        }

        public override void ExcuteTasks()
        {
            if (!SQLHelper.CanConnectToSQLServer(SqlServerConnString)) { ConsoleHelper.ShowException("无法连上SQLSERVER数据库"); return; }
            ExcuteTasks(true, true);
        }

        public void ExcuteTasks(bool creatTable = false, bool showProcess = false)
        {
            Console.WriteLine("任务数量：" + Tasks.Count());
            Console.WriteLine("数据库连接字符串：" + SqlServerConnString);
            if (!SQLHelper.CanConnectToSQLServer(SqlServerConnString)) { ConsoleHelper.ShowException("无法连上SQLSERVER数据库"); return; }
            if (creatTable)
            {
                foreach (var task in Tasks)
                {
                    string sql = string.Empty;
                    //GenerateTemplateSqlFile
                    var tableTemplateFile = Path.Combine(SqlsPath, task.Table + "Template.sql");
                    if (!File.Exists(tableTemplateFile))
                    {
                        var trueTableName = task.Table;                 //临时保存表名
                        task.Table = defaultTableName;            //生成模板文件，使用默认表名
                        var templateSql = GenerateCreateTableSql(task);
                        task.Table = trueTableName;
                        if (!string.IsNullOrEmpty(templateSql))
                        {
                            Console.WriteLine($"-->生成 {task.Table} 表模板 {tableTemplateFile} 成功！  " + DateTime.Now.ToString());
                            Directory.CreateDirectory(SqlsPath);
                            File.WriteAllText(tableTemplateFile, templateSql);  //语句非空才生成模板文件，否则空模板无法创建表
                            sql = templateSql.Replace(defaultTableName, task.Table); //替换为实际执行的表名，方便后面执行。
                        }
                        else
                        {
                            Console.WriteLine($"-->生成 {task.Table} 模板 {tableTemplateFile} 失败！  " + DateTime.Now.ToString());
                            sql = string.Empty;
                            continue;
                        }
                    }
                    else
                    {
                        Console.WriteLine($"-->已有 {task.Table} 表模板： {tableTemplateFile}  " + DateTime.Now.ToString());
                        sql = File.ReadAllText(tableTemplateFile).Replace(defaultTableName, task.Table);
                    }

                    //CreateDatabaseTable 如果表不存在（内含在语句中了）
                    if (!string.IsNullOrEmpty(sql))
                    {
                        SQLHelper.ExcuteSql(SqlServerConnString, sql);
                    }
                }
            }
            ExcuteTasks(showProcess);
        }

        public string GenerateCreateTableSql(SetDataTaskInfo task)
        {
            var fileToRemove = string.Empty;
            try
            {
                var encoding = TxtFileEncoder.GetEncoding(task.File);

                IEnumerable<dynamic> rows = null;
                List<string> columns = new List<string>();

                //csv 文件 换行方式有可能是 CRLF，CR或LF，此时可能出现异常，提示 给定关键字不在字典中。
                //我们如果想正常读取，则用Excel或wps 打开csv文档保存下，文档保存方式就变了，就可以正常读取了！！！
                if (task.ExcelType != ExcelType.CSV)
                {
                    ConsoleHelper.ShowException($"{task.File} 不是CSV文件");
                    return "";
                }
                else
                {
                    (task.Configuration as MiniExcelLibs.Csv.CsvConfiguration).StreamReaderFunc = (stream) => new StreamReader(stream, encoding);
                }

                var file = string.Empty;
                if (task.StartCell != "A1")    //如果首行不是标题，剪切csv文件，生成临时文件，去除前面多余行
                {
                    fileToRemove = file = GetCutedCsvFile(task, true);
                }
                else
                {
                    file = task.File;
                }
                columns = MiniExcel.GetColumns(file, true, "Sheet1", task.ExcelType, "A1", task.Configuration).ToList();
                rows = MiniExcel.Query(file, true, "Sheet1", task.ExcelType, "A1", task.Configuration).Take(100).ToList();
                CleanTempFile(fileToRemove);

                Dictionary<string, ColumnInfo> colsDic = new Dictionary<string, ColumnInfo>();
                columns.ForEach(columnName => colsDic.Add(columnName, new ColumnInfo(columnName))); //此处100为字段初始长度

                //// todo 遇到空白行则中断  暂时不做这功能
                //rows = rows.TakeWhile(row => ((IDictionary<string, object>)row).Take(3).All(cell => cell.Value != null));

                foreach (ExpandoObject row in rows.Take(100))
                {
                    //如果全部类型都设置了,不为null，则退出循环
                    if (colsDic.Values.All(n => n.Type != null)) break;
                    foreach (var cell in row.Where(c => c.Value != null))
                    {
                        var col = colsDic[cell.Key];
                        col.Type = cell.Value?.GetType();
                    }
                }

                #region to do

                //获取每列的最长长度,测试前1000行  先简单粗暴长度设置为max 不会报错，以后再细致研究长度。
                //foreach (ExpandoObject row in rows.Take(10000))
                //{
                //    foreach (var cell in row.Where(c => c.Value != null))
                //    {
                //        var colName = cell.Key;
                //        var col = colsDic[colName];
                //        //if (col.Type != typeof(string)) continue;

                //        var oldlength = col.Length;
                //        var newlength = cell.Value.ToString().Length;

                //        //var newlength = (cell.Value.ToString().Length * 2 / 200 + 1) * 200;  // 新长度该怎么增加？！！！ 可考虑优化
                //        if (newlength > oldlength)
                //        {
                //            col.Length = newlength * 2;
                //        }

                //    }
                //}

                #endregion to do

                string strSql = $"If Object_Id('{task.Table}') is null CREATE TABLE [dbo].[{task.Table}] (\n";
                foreach (var colName in columns)
                {
                    var col = colsDic[colName];

                    if (col.Type == typeof(double))
                    {
                        strSql += $"[{colName}] decimal(20,4),\n";
                    }
                    else
                    {
                        strSql += $"[{colName}] varchar(max),\n";  //col.Type 为 string 或 null 时
                    }
                }
                strSql = strSql.Trim(',') + ")\n";
                //Console.WriteLine(strSql);
                return strSql;
            }
            catch (Exception ex)
            {
                CleanTempFile(fileToRemove);
                ConsoleHelper.ShowException(ex.Message);
                return string.Empty;
            }
        }

        public void GetAllCutedCsvFiles(string FolderPath = "")
        {
            Tasks.Where(t => t.File.EndsWith(".csv", StringComparison.InvariantCultureIgnoreCase))
                .AsParallel().WithDegreeOfParallelism(Environment.ProcessorCount / 2).ForAll(task =>
            {
                try
                {
                    var cutedFile = new XlsxFileHelper().GetCutedFile(task.File, int.Parse(task.StartCell.Replace("A", "")), task.StopKeyString, false, FolderPath);
                    ConsoleHelper.ShowSuccess(cutedFile);
                }
                catch (Exception ex)
                {
                    ConsoleHelper.ShowException("处理失败： " + task.File + $"   原因为：{ex.Message}");
                }
            });
        }

        public void ShowTasksHistory(string table = "", string code = "", string file = "", string sheet = "", bool done = true)
        {
            IEnumerable<SetDataTaskInfo> alltasks = SqliteDb.Table<SetDataTaskInfo>().AsEnumerable();

            if (!string.IsNullOrEmpty(table))
            {
                alltasks = alltasks.Where(t => t.Table == table);
            }
            if (!string.IsNullOrEmpty(code))
            {
                alltasks = alltasks.Where(t => t.Code == code);
            }
            if (!string.IsNullOrEmpty(file))
            {
                alltasks = alltasks.Where(t => t.File == file);
            }
            if (!string.IsNullOrEmpty(sheet))
            {
                alltasks = alltasks.Where(t => t.Sheet == sheet);
            }

            alltasks = alltasks.Where(t => t.Done == done);
            alltasks.ToList().ForEach(t => Console.WriteLine(t.ToString()));
        }

        private static void CleanTempFile(string filetoRemove)
        {
            try { File.Delete(filetoRemove); }
            catch (Exception) { }
        }
        private static string GetCutedCsvFile(SetDataTaskInfo task, bool verySmall)
        {
            string preFileName = task.File;
            string cutedFileName = string.Empty;
            if (task.StartCell != "A1")
            {
                var startRow = int.Parse(task.StartCell.Replace("A", ""));
                cutedFileName = new XlsxFileHelper().GetCutedFile(preFileName, startRow, task.StopKeyString, verySmall);
            }
            return cutedFileName;
        }

        private void ExcuteTasks(bool showProcess = false)
        {
            using (SqlConnection connection = new SqlConnection(SqlServerConnString))
            {
                connection.Open();
                using (SqlBulkCopy bcp = new SqlBulkCopy(connection))
                {
                    bcp.BatchSize = 10000;//每次传输的行数,不设置，则全部回退本任务
                    bcp.BulkCopyTimeout = 0; //无限制，一直等

                    //tasks.AsParallel().WithDegreeOfParallelism(Environment.ProcessorCount / 2).ForAll(task =>
                    foreach (var task in Tasks) //顺序执行吧，没有并行运行的一些错误
                    {
                        task.Start = DateTime.Now;
                        Console.WriteLine($"\n开始导入：" + task.File + "   " + DateTime.Now.ToString());

                        SqlCommand commandRowCount = new SqlCommand($"SELECT COUNT(*) FROM dbo.{task.Table};");
                        commandRowCount.Connection = connection;

                        long countStart = -1;
                        try
                        {
                            countStart = Convert.ToInt32(commandRowCount.ExecuteScalar()); //有可能表不存在，报错
                        }
                        catch (Exception ex)
                        {
                            ConsoleHelper.ShowException($"\n导入失败：" + task.File + "\t==> " + task.Table + "   原因为：" + ex.Message);
                            continue;
                        }

                        #region todo

                        //Console.WriteLine($"<{task.Id}>【{task.Table}】表原有数据： {countStart} 行");
                        //没有意义，暂不统计。可根据拷贝结果验证。
                        //var rowsCount = MiniExcel.Query(task.File, true, task.Sheet).Count();
                        //Console.Write("-需拷贝行数：" + rowsCount);

                        #endregion todo

                        //列对应 若不对应则顺序拷贝 列顺序和数量必须一致。不可去除。
                        bcp.ColumnMappings.Clear(); //需要清理对应关系重新来过

                        //csv 文件 换行方式有可能是 CRLF，CR或LF，此时可能出现异常，提示 给定关键字不在字典中。
                        //我们如果想正常读取，则用Excel或wps 打开csv文档保存下，文档保存方式就变了，就可以正常读取了！！！

                        bcp.DestinationTableName = task.Table;//目标表
                        if (showProcess)
                        {
                            bcp.NotifyAfter = 10000;//进度提示的行数
                            bcp.SqlRowsCopied += new SqlRowsCopiedEventHandler(ShowProcess);
                        }
                        var filetoRemove = string.Empty;
                        try
                        {
                            if (task.ExcelType == ExcelType.CSV)
                            {
                                var encoding = TxtFileEncoder.GetEncoding(task.File);
                                (task.Configuration as MiniExcelLibs.Csv.CsvConfiguration).StreamReaderFunc = (stream) => new StreamReader(stream, encoding);
                            }
                            var file = string.Empty;

                            if (task.StartCell != "A1")    //如果首行不是标题，剪切csv文件，生成临时文件，去除前面多余行
                            {
                                filetoRemove = file = GetCutedCsvFile(task, false);
                            }
                            else
                            {
                                file = task.File;
                            }
                            var columns = MiniExcel.GetColumns(file, true, task.Sheet, task.ExcelType, "A1", task.Configuration).ToList();
                            columns.ForEach(columnName => bcp.ColumnMappings.Add(new SqlBulkCopyColumnMapping(columnName, columnName)));
                            using (var reader = MiniExcel.GetReader(file, true, task.Sheet, task.ExcelType, "A1", task.Configuration))
                            {
                                bcp.WriteToServer(reader);
                            }
                            task.End = DateTime.Now;
                            task.Done = true;
                            if (countStart == -1) //说明有错误发生，未查询到原表数据
                            {
                                Console.WriteLine($"{task.Table}未查询到原表数据,请确认是否存在表！ " + DateTime.Now.ToString());
                            }
                            else
                            {
                                long countEnd = Convert.ToInt32(commandRowCount.ExecuteScalar());
                                task.Detail = $"导入{(countEnd - countStart)}行,现有{countEnd} 行";
                                Console.WriteLine($"{task.Table}导入{(countEnd - countStart)}行,现有{countEnd} 行 " + DateTime.Now.ToString());
                            }

                            //CleanTempFile(filetoRemove);

                            // todo 遇到空白则中断 暂不做此功能，此处会报错！
                            //bcp.WriteToServer(QueryWithEmptyRowBreak(new FileStream(task.File, FileMode.Open), true, task.Sheet, excelType, task.StartCell, configuration: config) as DataRow[]);

                        }
                        catch (Exception ex)
                        {
                            //CleanTempFile(filetoRemove);
                            task.Done = false;
                            task.Detail = ex.Message;
                            ConsoleHelper.ShowException($"{ex.Message}\n<{task.Id}>【{task.Table}】导入失败，回退【{task.File}】本次导入！");
                        }
                    }
                }
            }
            SqliteDb.InsertAll(Tasks);
            Console.WriteLine("\n导入结果：");
            Tasks.ForEach(task => { Console.WriteLine(task.ToString()); });
        }

        private void ShowProcess(object sender, SqlRowsCopiedEventArgs e)
        {
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write("-->已拷贝行：" + e.RowsCopied.ToString());
        }
    }
}