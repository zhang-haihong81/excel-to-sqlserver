﻿using ExcelToSqlserver.Helpers;

using MiniExcelLibs;

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;

namespace ExcelToSqlserver
{
    // Database Table ==> Excel sheet
    internal class GetDataTaskManager : TaskManager<GetDataTaskInfo>
    {
        protected string ExcelsPath;

        public GetDataTaskManager() : base()
        {
            string basePath = AppDomain.CurrentDomain.BaseDirectory;
            ExcelsPath = Path.Combine(basePath, "Excels");
            ExcelSheetName = "GetDataTasks";
            Tasks = MiniExcel.Query<GetDataTaskInfo>(Path.Combine(basePath, TasksExcelFile), ExcelSheetName).Where(t => t.ShouldDo).ToList();
            SqliteDb.CreateTable<GetDataTaskInfo>();
        }

        public override void ExcuteTasks()
        {
            Console.WriteLine("\n开始执行Tasks.xlsx中任务！");
            Directory.CreateDirectory(ExcelsPath);
            Tasks.AsParallel().WithDegreeOfParallelism(Environment.ProcessorCount / 2).ForAll(t => t.Start = DateTime.Now);
            try
            {
                Tasks.GroupBy(t => t.File).AsParallel().ForAll(f =>
                {
                    var sheets = new Dictionary<string, object>();
                    f.AsParallel().WithDegreeOfParallelism(Environment.ProcessorCount / 2).ForAll(task =>
                            {
                                task.Start = DateTime.Now;
                                SqlConnection sqlconn = new SqlConnection(SqlServerConnString);
                                sqlconn.Open();
                                SqlCommand command = sqlconn.CreateCommand();
                                if (!string.IsNullOrEmpty(task.SqlFile) && File.Exists(task.SqlFile))
                                {
                                    command.CommandText = File.ReadAllText(task.SqlFile);
                                }
                                else
                                {
                                    command.CommandText = task.Sql;
                                }
                                sheets.Add(task.Sheet, command.ExecuteReader());
                                //sqlconn.Close(); //此处不能关闭，否则将出现错误：发生一个或多个错误。
                            });
                    MiniExcel.SaveAs(Path.Combine(ExcelsPath, f.Key), sheets, true, overwriteFile: true);

                    Console.WriteLine($"{f.Key} 生成完毕！");
                });
                Tasks.ForEach(t => t.End = DateTime.Now);
                Tasks.ForEach(t => t.Done = true);
            }
            catch (Exception ex)
            {
                ConsoleHelper.ShowException(ex.Message);
            }
            Tasks.ForEach(task => { Console.WriteLine(task.ToString()); });
            SqliteDb.InsertAll(Tasks);
        }
    }
}