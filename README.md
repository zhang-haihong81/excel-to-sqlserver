# ExcelToSqlserver

#### 介绍
小巧快速的Excel导入到sqlserver程序，支持自定义任务：excel sheet table

#### 软件架构
Console


#### 使用说明

1.  settings 中配置数据库链接参数及excel表sheetName与数据库表的对应关系
2.  ExcelToSqlserver -g 创建sql脚本及模板（可不执行本步骤）
3.  ExcelToSqlserver 数据库无表则创建表，并导入数据

#### 参与贡献

审计信息化群：256912702